#!/bin/bash -e

HOME_DIR="/home/dev"
DISTRO=""

check_distro() {
	if type yum >/dev/null 2>&1; then
		DISTRO="redhat"
	fi
	if type apt-get >/dev/null 2>&1; then
		DISTRO="debian"
	fi
}

install_dependencies() {
	check_distro

	if [ $DISTRO == "redhat" ]; then
		sudo yum update
		sudo yum install python-setuptools git
		sudo yum install -y httpd postgresql postgresql-server postgresql-devel python-devel mod_wsgi
		sudo yum install -y screen
	fi

	if [ $DISTRO == "debian" ]; then
		sudo apt-get update
		sudo apt-get install python-setuptools
		sudo apt-get install -y httpd postgresql postgresql-server postgresql-devel python-devel mod_wsgi
		sudo apt-get install -y screen
	fi
}

init_postgres() {
	sudo service postgresql initdb
	sudo chkconfig --add postgresql
	sudo chkconfig postgresql on
	sudo service postgresql start
	sudo -u postgres -H bash -c "createdb active_citizen"
	sudo -u postgres -H bash -c "createuser dev"
	# Edit pg_hba.conf
	ed /var/lib/pgsql/data/pg_hba.conf
}

init_virtualenv() {
	sudo pip install virtualenvwrapper
	source virtualenvwrapper.sh
	mkvirtualenv ac
	add2virtualenv $HOME_DIR/active-citizen
	add2virtualenv $HOME_DIR/active-citizen/active_citizen2/apps
}

# Should only be ran inside virtualenv
install_pinaxutils() {
	pushd .
	cd /tmp && git clone git://github.com/pinax/pinax-utils.git
	cd pinax-utils && python setup.py install
	cd ../ && rm -rf pinax-utils
	popd
}

init_django() {
	git clone git@bitbucket.org:9diov/active-citizen.git
	init_virtualenv
	workon ac
	install_pinaxutils
	cd $HOME_DIR/active-citizen && pip install -r requirements.txt
	cd $HOME_DIR/active-citizen && python manage.py syncdb
	cd $HOME_DIR/active-citizen && python manage.py collectstatic --noinput
}

init_httpd() {
	# Fix 403 forbidden error
	chmod o+x $HOME_DIR
	cd $HOME_DIR/active-citizen
}

install_revotwitter() {
	cd $HOME_DIR/active-citizen
	git submodule init
	git submodule update
	cd $HOME_DIR/active-citizen/lib/RevoTwitter
}

