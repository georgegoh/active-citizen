import psycopg2, twitter, time, daemon, logging, os

def retrieve_keywords():
    words = []
    try:
        conn = psycopg2.connect(database='active_citizen', user='dev')
        cur = conn.cursor()
        cur.execute('select value from trends_keyword')
        words = cur.fetchall()
        words = [elem[0].decode('utf-8') for elem in words]
        words = [word for word in words if all(ord(c) < 128 for c in word)] # Only ascii character words
    except psycopg2.DatabaseError, e:
        print e
        if conn:
            conn.rollback()
    finally:
        if conn:
            conn.close()

    return words

def save_to_db(data_stream):
    logging.basicConfig(filename="geo_stream.log", level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')
    try:
        logging.info("Connecting to database...")
        conn = psycopg2.connect(database='active_citizen', user='dev')
        logging.info("Connected.")
        cur = conn.cursor()
        count = 0
        for tweet in data_stream:
            print tweet
            cur.execute("""insert into "geo_tweets" values (%s,%s,%s,%s,%s,%s);""", (
                str(tweet[u'favorited']).lower().encode('utf-8'),
                str(tweet[u'truncated']).lower().encode('utf-8'),
                tweet[u'id'],
                tweet[u'text'].encode('utf-8'),
                tweet[u'user'][u'screen_name'].encode('utf-8'),
                time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet[u'created_at'],'%a %b %d %H:%M:%S +0000 %Y'))))
            conn.commit()
            if count == 100:
                logging.info("%d tweets recorded since last time" % count)
                count = 0;
            count += 1
    except psycopg2.DatabaseError, e:
        logging.error(e)
        if conn:
            conn.rollback()
    finally:
        if conn:
            conn.close()


def geostream():
    while True:
        try:
            stream = twitter.TwitterStream(auth=twitter.OAuth('1240807442-mfWTYSYplvy6osAFTveCrPztJCK5MzNhMFcAwW8', 'tTiu8onMHQN8eoH4uxkkbt0NyAW9xZZGtJXiu1eWc', 'dxhHGSU642GHHT7FA', 'OyAwAKbk64FiCifWeihlSl23419NLQxrIWNFj2L37rA'))

            data_stream = stream.statuses.filter(language='en', locations='103.545,1.156,104.108,1.472') # Singapore bounding boxes
            save_to_db(data_stream)
        except Exception, e:
			logging.error(e)
			time.sleep(300)
			continue

if __name__=='__main__':
    working_directory = os.path.dirname(os.path.realpath(__file__))
    with daemon.DaemonContext(working_directory=working_directory):
        geostream()
