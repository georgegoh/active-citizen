import twitter, time, psycopg2


class ConnectionError(Exception):
    pass

class RateLimitError(Exception):
    pass

class TwitterSearch(object):
    def __init__(self):
        self.conn = None
        self.limit_next_window = None
        self.limit_remaining = None
        self.data = {}

    def _connect(self):
        self.conn = twitter.Twitter(auth=twitter.OAuth('1240807442-mfWTYSYplvy6osAFTveCrPztJCK5MzNhMFcAwW8', 'tTiu8onMHQN8eoH4uxkkbt0NyAW9xZZGtJXiu1eWc', 'dxhHGSU642GHHT7FA', 'OyAwAKbk64FiCifWeihlSl23419NLQxrIWNFj2L37rA'))

    def _rate_limited(self, http_call=True):
        if http_call:
            if not self.conn:
                raise ConnectionError('Twitter authentication session must be created first')

            rate_limit = self.conn.application.rate_limit_status()
            self.limit_remaining = int(rate_limit[u'resources'][u'search'][u'/search/tweets'][u'remaining'])
            self.limit_next_window = int(rate_limit[u'resources'][u'search'][u'/search/tweets'][u'reset'])

        #print "Limit remaining %d" % self.limit_remaining
        #print "Next limit window in %d seconds" % (self.limit_next_window - int(time.time()))
        if self.limit_remaining > 0:
            return False

        return True

    def _parse_rate_limit(self, data):
        self.limit_remaining = int(data.headers.getheader('x-rate-limit-remaining'))
        self.limit_next_window = int(data.headers.getheader('x-rate-limit-reset'))

    def search(self, query, lang='en', max_tweets=None):
        self._connect()
        if self._rate_limited():
            cur_time = int(time.time())
            raise RateLimitError('Rate limit exceeded. Please wait for %d seconds before searching again' % (self.limit_next_window - cur_time))

        first_time = True
        max_id, count, since_id = 0, 50, 0
        all_data = []
        while True:
            if self._rate_limited(http_call=False):
                raise RateLimitError('Rate limit exceeded. Please wait for %d seconds before searching again' % (self.limit_next_window - cur_time))
            raw_data={}
            if first_time:
                raw_data = self.conn.search.tweets(q=query, lang=lang, count=count)
                first_time = False
            else:
                max_id = since_id - 1
                raw_data = self.conn.search.tweets(q=query, lang=lang, count=count, max_id=max_id)

            self._parse_rate_limit(raw_data)

            max_id = raw_data[u'search_metadata'][u'max_id']
            since_id = raw_data[u'statuses'][-1]['id']
            for tweet in raw_data[u'statuses']:
                print tweet[u'id']

            if max_tweets is not None:
                if len(all_data) + len(raw_data[u'statuses']) > max_tweets:
                    all_data+=raw_data[u'statuses'][:(max_tweets - len(all_data))]
                    break
                else:
                    all_data+=raw_data[u'statuses']
            else:
                all_data+=raw_data[u'statuses']

            if 'next_results' not in raw_data[u'search_metadata'].keys(): # no more results
                break

        self.data[query] = all_data
        print len(all_data)
        return all_data

    def save_to_db(self, conn):
        cur = conn.cursor()
        for k, v in self.data.iteritems():
            for elem in v:
                cur.execute("""insert into "tweets" values ( %s, %s, NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s);""", (elem[u'text'].encode('utf-8'),
                    str(elem[u'favorited']).lower().encode('utf-8'),
                    time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(elem[u'created_at'],'%a %b %d %H:%M:%S +0000 %Y')),
                    str(elem[u'truncated']).lower().encode('utf-8'),
                    '' if elem[u'in_reply_to_status_id_str'] is None else elem[u'in_reply_to_status_id_str'].encode('utf-8'),
                    elem[u'id'],
                    elem[u'in_reply_to_user_id'],
                    elem[u'source'].encode('utf-8'),
                    elem[u'user'][u'screen_name'].encode('utf-8'),
                    0, k))
        conn.commit()

class DBConnection(object):
    def __init__(self, database, user):
        self.database = database
        self.user = user

    def __enter__(self):
        self.conn = psycopg2.connect(database=self.database, user=self.user)
        return self.conn

    def __exit__(self, ex_type, ex_value, ex_traceback):
        if ex_type is psycopg2.DatabaseError:
            print ex_value
            self.conn.rollback()
        self.conn.close()
        return True


if __name__=='__main__':
    ts = TwitterSearch()
    ts.search(query="china", max_tweets=50)
    with DBConnection(database='active_citizen', user='dev') as conn:
        ts.save_to_db(conn)
