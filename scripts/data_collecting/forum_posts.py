import feedparser
import lxml.html
import time, os
import psycopg2, logging, daemon

def store_to_db(entries):
    try:
        logging.info("Connecting to database...")
        conn = psycopg2.connect(database='active_citizen', user='dev')
        logging.info("Connected.")
        cur = conn.cursor()
        cur.execute("""drop function if exists fn_update_forum_posts(source text, id text, author text, published timestamp without time zone, text text);""")
        cur.execute("""
            create function fn_update_forum_posts(source text, id text, author text, published timestamp without time zone, text text) returns void
            as
            $$
                insert into "forum_posts" (source, id, author, published, text)
                select $1,$2,$3,$4,$5
                where $2 not in (select id from "forum_posts");
            $$
            language 'sql';""")
        count = 0
        for entry in entries:
            source = "http://forums.hardwarezone.com.sg/"
            published = time.strftime('%Y-%m-%d %H:%M:%S', entry.published_parsed)
            text = u'\n'.join([entry.title, lxml.html.fromstring(entry.content[0].value).text_content()])
            cur.execute("""select fn_update_forum_posts(%s,%s,%s,%s,%s)""", (
                source,
                entry.id,
                entry.author,
                published,
                text))
            conn.commit()
            if count == 100:
                logging.info("%d tweets recorded since last time" % count)
                count = 0;
            count += 1
    except psycopg2.DatabaseError, e:
        logging.error(e)
        if conn:
            conn.rollback()
    finally:
        if conn:
            conn.close()

def retrieve_data():
    logging.basicConfig(filename="forum_stream.log", level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')
    logging.info("Starting script...")
    url = 'http://forums.hardwarezone.com.sg/external.php?type=RSS2&forumids=16'
    logging.info("Retrieving feed...")
    data = feedparser.parse(url)
    while True:
        logging.info("HTTP status = %d" % data.status )
        if data.status == 200:
            logging.info("Storing to db:")
            store_to_db(data.entries)
        time.sleep(300)
        data = feedparser.parse(url, etag = data.etag)

if __name__ == '__main__':
    working_directory = os.path.dirname(os.path.realpath(__file__))
    with daemon.DaemonContext(working_directory=working_directory):
        retrieve_data()
