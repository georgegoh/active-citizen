#! /usr/bin/R
##
##Mini crontab twitter
######################

Sys.time()

library(RevoTwitter)

library(RPostgreSQL)
conn <- dbConnect(dbDriver("PostgreSQL"), dbname="active_citizen", user="dev", host="localhost")
google_query <- 'select value from trends_keyword where source=\'google\''
twitter_query <- 'select value from trends_keyword where source=\'twitter\''
user_query <- 'select value from trends_userkeyword'
google_keywords <- dbGetQuery(conn, google_query) 
twitter_keywords <- dbGetQuery(conn, twitter_query) 
user_keywords <- dbGetQuery(conn, user_query)
dbDisconnect(conn)

initializeOrUpdateKeyword <- function(keyword) {
	cat("Processing ", keyword, " ...")
	conn <- dbConnect(dbDriver("PostgreSQL"), dbname="active_citizen", user="dev", host="localhost")
	tryCatch({ initializeFeed(conn, keyword) }, error=function(err) { paste('Error: ', err) })
	dbDisconnect(conn)
	conn <- dbConnect(dbDriver("PostgreSQL"), dbname="active_citizen", user="dev", host="localhost")
	tryCatch({ updateFeed(conn, keyword) }, error=function(err) { paste('Error: ', err) })
	dbDisconnect(conn)
}

lapply(google_keywords[['value']], initializeOrUpdateKeyword)
lapply(twitter_keywords[['value']], initializeOrUpdateKeyword)
#lapply(user_keywords[['value']], initializeOrUpdateKeyword)


