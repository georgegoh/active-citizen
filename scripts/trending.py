#!/usr/bin/env python

# Getting Singapore's trending keywords from Google Trends and Twitter
# Dependencies: BeautifulSoup, oauth2

import urllib
import urllib2
import re
import csv

from cookielib import CookieJar

class pyGTrends(object):
    """
    Google Trends API

    Recommended usage:

    from csv import DictReader
    r = pyGTrends(username, password)
    r.download_report(('pants', 'skirt'))
    d = DictReader(r.csv().split('\n'))
    """
    def __init__(self, username, password):
        """
        provide login and password to be used to connect to Google Analytics
        all immutable system variables are also defined here
        website_id is the ID of the specific site on google analytics
        """
        self.login_params = {
            "continue": 'http://www.google.com/trends',
            "PersistentCookie": "yes",
            "Email": username,
            "Passwd": password,
        }
        self.headers = [("Referrer", "https://www.google.com/accounts/ServiceLoginBoxAuth"),
                        ("Content-type", "application/x-www-form-urlencoded"),
                        ('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21'),
                        ("Accept", "text/plain")]
        self.url_ServiceLoginBoxAuth = 'https://accounts.google.com/ServiceLoginBoxAuth'
        self.url_Export = 'http://www.google.com/trends/viz'
        self.url_CookieCheck = 'https://www.google.com/accounts/CheckCookie?chtml=LoginDoneHtml'
	self.url_PrefCookie = 'http://www.google.com'
        self.header_dictionary = {}
        self._connect()

    def _connect(self):
        """
        connect to Google Trends
        """

        self.cj = CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        self.opener.addheaders = self.headers

        galx = re.compile('<input type="hidden" name="GALX" value="(?P<galx>[a-zA-Z0-9_-]+)">')

        resp = self.opener.open(self.url_ServiceLoginBoxAuth).read()
        resp = re.sub(r'\s\s+', ' ', resp)
        m = galx.search(resp)
        if not m:
            raise Exception("Cannot parse GALX out of login page")
        self.login_params['GALX'] = m.group('galx')
        params = urllib.urlencode(self.login_params)
        self.opener.open(self.url_ServiceLoginBoxAuth, params)
        self.opener.open(self.url_CookieCheck)
        self.opener.open(self.url_PrefCookie)

    def download_report(self, keywords, date='all', geo='all', geor='all', graph
            = 'all_csv', sort=0, scale=0, sa='N', params=''):
        """
        download a specific report
        date, geo, geor, graph, sort, scale and sa
        are all Google Trends specific ways to slice the data
        """
        if type(keywords) not in (type([]), type(('tuple',))):
            keywords = [keywords]

        if params == '':
            params = urllib.urlencode({
                'q': ",".join(keywords),
                'date': date,
                'graph': graph,
                'geo': geo,
                'geor': geor,
                'sort': str(sort),
                'scale': str(scale),
                'sa': sa
            })
        else:
            params = urllib.urlencode(params)
        #self.raw_data = self.opener.open('http://www.google.com/trends/viz?' + params).read()


        url = 'http://www.google.com/trends/fetchComponent?' + params
        #url = 'https://www.google.com/trends/trendsReport?hl=en-US&cat=0-16&geo=SG&date=today%207-d&cmpt=q&content=1&export=1'
        print url
        self.raw_data = self.opener.open(url).read()
        if self.raw_data in ['You must be signed in to export data from Google Trends']:
            raise Exception(self.raw_data)

    def csv(self, section="main", as_list=False):
        """
        Returns a CSV of a specific segment of the data.
        Available segments include Main, Language, City and Region.
        """
        if section == "main":
            section = ("Week","Year","Day","Month")
        else:
            section = (section,)

        segments = self.raw_data.split('\n\n\n')
        for s in segments:
            if s.partition(',')[0] in section:
                if as_list:
                    return [line for line in csv.reader(s.split('\n'))]
                else:
                    return s

        raise Exception("Could not find requested section")


"""
soupselect.py

CSS selector support for BeautifulSoup.

soup = BeautifulSoup('<html>...')
select(soup, 'div')
- returns a list of div elements

select(soup, 'div#main ul a')
- returns a list of links inside a ul inside div#main

"""

tag_re = re.compile('^[a-z0-9]+$')

attribselect_re = re.compile(
    r'^(?P<tag>\w+)?\[(?P<attribute>\w+)(?P<operator>[=~\|\^\$\*]?)' +
    r'=?"?(?P<value>[^\]"]*)"?\]$'
)

# /^(\w+)\[(\w+)([=~\|\^\$\*]?)=?"?([^\]"]*)"?\]$/
#   \---/  \---/\-------------/    \-------/
#     |      |         |               |
#     |      |         |           The value
#     |      |    ~,|,^,$,* or =
#     |   Attribute
#    Tag

def attribute_checker(operator, attribute, value=''):
    """
    Takes an operator, attribute and optional value; returns a function that
    will return True for elements that match that combination.
    """
    return {
        '=': lambda el: el.get(attribute) == value,
        # attribute includes value as one of a set of space separated tokens
        '~': lambda el: value in el.get(attribute, '').split(),
        # attribute starts with value
        '^': lambda el: el.get(attribute, '').startswith(value),
        # attribute ends with value
        '$': lambda el: el.get(attribute, '').endswith(value),
        # attribute contains value
        '*': lambda el: value in el.get(attribute, ''),
        # attribute is either exactly value or starts with value-
        '|': lambda el: el.get(attribute, '') == value \
            or el.get(attribute, '').startswith('%s-' % value),
    }.get(operator, lambda el: el.has_key(attribute))

def select(soup, selector):
    """
    soup should be a BeautifulSoup instance; selector is a CSS selector
    specifying the elements you want to retrieve.
    """
    tokens = selector.split()
    current_context = [soup]
    for token in tokens:
        m = attribselect_re.match(token)
        if m:
            # Attribute selector
            tag, attribute, operator, value = m.groups()
            if not tag:
                tag = True
            checker = attribute_checker(operator, attribute, value)
            found = []
            for context in current_context:
                found.extend([el for el in context.findAll(tag) if checker(el)])
            current_context = found
            continue
        if '#' in token:
            # ID selector
            tag, id = token.split('#', 1)
            if not tag:
                tag = True
            el = current_context[0].find(tag, {'id': id})
            if not el:
                return [] # No match
            current_context = [el]
            continue
        if '.' in token:
            # Class selector
            tag, klass = token.split('.', 1)
            if not tag:
                tag = True
            found = []
            for context in current_context:
                found.extend(
                    context.findAll(tag,
                        {'class': lambda attr: attr and klass in attr.split()}
                    )
                )
            current_context = found
            continue
        if token == '*':
            # Star selector
            found = []
            for context in current_context:
                found.extend(context.findAll(True))
            current_context = found
            continue
        # Here we should just have a regular tag
        if not tag_re.match(token):
            return []
        found = []
        for context in current_context:
            found.extend(context.findAll(token))
        current_context = found
    return current_context

def monkeypatch(BeautifulSoupClass=None):
    """
    If you don't explicitly state the class to patch, defaults to the most
    common import location for BeautifulSoup.
    """
    if not BeautifulSoupClass:
        from BeautifulSoup import BeautifulSoup as BeautifulSoupClass
    BeautifulSoupClass.findSelect = select

def unmonkeypatch(BeautifulSoupClass=None):
    if not BeautifulSoupClass:
        from BeautifulSoup import BeautifulSoup as BeautifulSoupClass
    delattr(BeautifulSoupClass, 'findSelect')

"""
Twitter local trends
"""
import oauth2 as oauth

def oauth_req(url, key, secret, http_method="GET", post_body=None,
        http_headers=None):
    consumer = oauth.Consumer(key='fU5zTA6HOLn2pxOQo6LQ', secret='PquUV0ECfokBGcu8SZdXNBXGAe4QJkqC3cq2Rz2s')
    token = oauth.Token(key=key, secret=secret)
    client = oauth.Client(consumer, token)

    resp, content = client.request(
        url,
        method=http_method,
    )
    return content

def get_twitter_local_trends(woeid='23424948'): # Singapore YWOEID
    trends = oauth_req(
      'https://api.twitter.com/1.1/trends/place.json?id=' + woeid,
      '10195032-OCPTT9izIOZGjgJ284hGP2R9ALsRX9J0JvaakWCZ9',
      'UQN7nVdiz7vMW2Xd6okFs9R3LJuTkUtzeUhIBSPw'
    )

    import json
    trends = json.loads(trends)
    return [trend['name'] for trend in trends[0]['trends']]

def get_google_local_trends():
    connector = pyGTrends('gtrendsbot@gmail.com', 'kickingasses')
    connector.download_report('', params={'cmpt':'q', 'content':'1', 'cid':'RISING_QUERIES_0_0', 'export':'5','date':'today 7-d', 'geo':'SG'})
    from BeautifulSoup import BeautifulSoup as Soup

    soup = Soup(connector.raw_data)
    s = select(soup, '.trends-bar-chart-name-cell a')

    keywords = [item.contents[0] for i, item in enumerate(s) if i % 2 ==0]
    return keywords

def clean_up_db():
    import sqlite3
    conn = sqlite3.connect('/mnt/data/keywords.db')
    delete_keywords_query = """DELETE FROM KEYWORDS"""
    conn.execute(delete_keywords_query)
    conn.commit()
    conn.close()

def store_to_db(source='', keywords=None):
    if source == '' or keywords == None:
        return
    import sqlite3
    conn = sqlite3.connect('/mnt/data/keywords.db')
    create_table_query = """CREATE TABLE IF NOT EXISTS KEYWORDS (SOURCE TEXT, KEYWORD TEXT);"""
    conn.execute(create_table_query)

    insert_keywords_query = """INSERT INTO KEYWORDS VALUES (?, ?)"""
    conn.executemany(insert_keywords_query, keywords)
    conn.commit()
    conn.close()


if __name__ == "__main__":
    google_keywords = get_google_local_trends()
    print google_keywords
    twitter_keywords = get_twitter_local_trends()
    print twitter_keywords
    #clean_up_db()
    #store_to_db('google', [('google', value) for value in google_keywords])
    #store_to_db('twitter', [('twitter', value) for value in twitter_keywords])


