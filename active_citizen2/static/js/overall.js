$.overall = {
    data:[],
    updateOverallGraph: function(chartId) {
        nv.addGraph(function() {
            //var chart = nv.models.discreteBarChart();
            var chart = nv.models.lineChart();
            chart.xAxis.tickFormat(function(d) {
                return d3.time.format('%d/%m/%y %H:%M')(new Date(d));
            });

            chart.yAxis.tickFormat(d3.format(',d'));

            chart.x(function(d) { return d.x })
            .y(function(d) { return d.y })
            //.staggerLabels(true)
            .tooltips(true)
            //.showValues(true)

            d3.select(chartId + ' svg').remove();
            d3.select(chartId).append('svg:svg').attr("id", function() { return "overall_graph"; });

            d3.select(chartId + ' svg')
            .datum($.overall.data)
            .transition().duration(500)
            .call(chart);

            nv.utils.windowResize(chart.update);

            return chart;
         });
    }
};
