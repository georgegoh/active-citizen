$(function() {
	$('#search_input').focus();
	$("#search_input").keyup(function(event){
		if(event.keyCode == 13 && $.trim($('#search_input').val()) != ""){
			$("#analyze_btn").click();
		}
	});
	var $output_wordcloud = $('#wordcloud_div');
	var $output_graphing = $('#relation_graph');
	var $output_overall = $('#overall_div');
	var $output_sentiment = $('#sentiment_div');
	var showLoading = function() {
		$("#wordcloud_div > img").show();
		$("#overall_div > img").show();
		$("#igraph_div > img").show();
		$("#sentiment_div > img").show();
		$("#wordcloud_div > p").remove();
		$("#overall_div > p").remove();
		$("#relation_graph > p").remove();
		$("#sentiment_div > p").remove();
		$("#wordcloud_graph").remove();
		$("#overall_graph").remove();
		$("#relation_graph").remove();
		$("#sentiment_graph").remove();
	}
	var hideLoading = function() {
		$("#wordcloud_div > img").hide();
		$("#overall_div > img").hide();
		$("#igraph_div > img").hide();
		$("#sentiment_div > img").hide();
	}
	var error = function(e) { console.log(e); }

	var error = function(e) {
		console.log("Error " + e.deployr.response.errorCode + ": " + e.deployr.response.error);
	};
	var errorFunc = function(outputNode) {
		return function(e) {
			outputNode.children("img").hide();
			outputNode.prepend("<p>" + "Error " + e.deployr.response.errorCode + ": " + e.deployr.response.error + "</p>");
			console.log("Error " + e.deployr.response.errorCode + ": " + e.deployr.response.error);
		};
	};

	var logIn = function(username, password, callbackFunc) {
		if (window.project) { callbackFunc(); return; }

		R.DeployR.init({
			events: {
				// -- close open project and log out on browser close --
				unload: {
					disableautosave: true,
					projecttcookie: '',
					space: true,
					dropdirectory: true,
					drophistory: true,
					flushhistory: true
				}
			}
		});

		var logInSuccess = function(response) {
			var callback = {
				scope: this,
				success: function(r) {
					window.project = r.deployr.response.project.project;
					callbackFunc();
				},
				failure: error
			};
			console.log("Successfully logged in!");
			R.DeployR.projectCreate({}, callback);
		};

		var loginCallback = { success: logInSuccess, failure: error, scope: this };
		//R.DeployR.userLogout({}, function() {});
		R.DeployR.userLogin({ username: username, password: password, disableautosave: true}, loginCallback);
	};

	var executeCode = function(code) {
		showLoading();

		var complete = function(result) {
			var output = result.deployr.response.execution.console;
			$output.text(output.replace(/\n/g, '&ltbr&gt'));

			$output.text(output);
		};

		var callback = {
			success: complete,
			failure: error,
			scope: this
		};

		var codeConfig = {
			project: window.project, code: code
		};

		R.DeployR.projectExecuteCode(codeConfig, callback);
	};

	var genericComplete = function(result, scriptName, outputNode) {
		try {
			var url = result.deployr.response.execution.artifacts[0].url;
		} catch (e) {
			console.log(e.message);
		}
		if (!url) {
			var output = '<p>' + result.deployr.response.error + '</p>';
			if (outputNode != null) {
				outputNode.html(output);
			}
			return;
		}
		var output = '<img src="' + url.replace(/(localhost|127.0.0.1)/, window.location.hostname) + '">';

		$("#submit-status").html("<b>Status:</b> " + scriptName + " execution done");
		if (outputNode != null) {
			outputNode.html(output);
		}
		if (result.deployr.response.warnings) {
			console.log(result.deployr.response.warnings);
		}
	};
	var executeScript = function(scriptName, inputs, robjects, outputNode, callbackFunc) {
		complete = genericComplete;
		if (callbackFunc != null) {
			complete = callbackFunc(scriptName, outputNode);
		}

		var error = function(e) {
			$("#submit-status").html("<b>Status:</b> " + 'Error: ' + e.deployr.response.error);
			if (outputNode != null) {
				outputNode.html('<p>Failed to execute script: ' +  e.deployr.response.error + '</p>');
			}
		};

		var callback = {
			success: complete,
			failure: errorFunc(outputNode),
			scope: this
		};

		var codeConfig = (robjects) ? {
			project: window.project,
			rscript: scriptName,
			robjects: robjects,
			inputs: inputs
		} : {
			project: window.project,
			rscript: scriptName,
			inputs: inputs
		};

		R.DeployR.repositoryScriptExecute(codeConfig, callback);
	};

	var wordcloudComplete = function(scriptName, outputNode) {
		var zipWordCloudData = function(arr1, arr2) {
			return arr1.map(function(elem, index) {
				return {"text": elem, "size": arr2[index]};
			});
		};
		return function(result) {
			$("#wordcloud_div > img").hide();
			$("#wordcloud_div > p").remove();
			$.wordcloud.data = zipWordCloudData(result.deployr.response.workspace.objects[0].value.word.value.slice(0, 300), result.deployr.response.workspace.objects[0].value.freq.value.slice(0, 300));
			$.wordcloud.update_wordcloud();

			$("#submit-status").html("<b>Status:</b> " + scriptName + " execution done");
			if (result.deployr.response.warnings) {
				console.log(result.deployr.response.warnings);
			}
		};
	};

	var overallComplete = function(scriptName, outputNode) {
		var zipOverallData = function(arr1, arr2) {
			return arr1.map(function(elem, index) {
				return {"x": elem, "y": arr2[index]};
			});
		};
		return function(result) {
			$("#overall_div > img").hide();
			$("#overall_div > p").remove();
			$.overall.data = [{
				"key": "Number of tweets per hour",
				"values": zipOverallData(result.deployr.response.workspace.objects[0].value.created.value, result.deployr.response.workspace.objects[0].value.V1.value)
			}];
			$.overall.updateOverallGraph('#overall_div');

			$("#submit-status").html("<b>Status:</b> " + scriptName + " execution done");
			if (result.deployr.response.warnings) {
				console.log(result.deployr.response.warnings);
			}
		};
	};

	var igraphComplete = function(scriptName, outputNode) {
        var prepareRelationData = function(data) {
            return data.map(function(elem, index) {
                return {"source": elem[0], "target": elem[1]};
            });
        }
		return function(result) {
			$("#igraph_div > img").hide();
			$("#igraph_div > p").remove();
            try {
                $.relation_graph.data = prepareRelationData(result.deployr.response.workspace.objects[0].value);
                $.relation_graph.update_relation_graph();
            } catch (e) {
                console.log(e.message);
            }

			//try {
				//var url = result.deployr.response.execution.artifacts[0].url;
			//} catch (e) {
				//console.log(e.message);
			//}
			//if (!url) {
				//var output = '<p>' + result.deployr.response.error + '</p>';
				//if (outputNode != null) {
					//outputNode.html(output);
				//}
				//return;
			//}
			//var output = '<img src="' + url.replace(/(localhost|127.0.0.1)/, window.location.hostname) + '">';

			//$("#submit-status").html("<b>Status:</b> " + scriptName + " execution done");
			if (outputNode != null) {
				outputNode.html(output);
				outputNode.show();
			}
			if (result.deployr.response.warnings) {
				console.log(result.deployr.response.warnings);
			}
		};
	};

	var sentimentComplete = function(scriptName, outputNode) {
		var zipSentimentData = function(arr1, arr2) {
			return arr1.map(function(elem, index) {
				return {"x": elem, "y": arr2[index]};
			});
		};
		return function(result) {
			$("#sentiment_div > img").hide();
			$("#sentiment_div > p").remove();
			$.sentiment.data = [{
				"key": "sentimentGraph",
				"values": zipSentimentData(result.deployr.response.workspace.objects[0].value.created.value, result.deployr.response.workspace.objects[0].value.smoothed.value)
			}];
			$.sentiment.updateSentimentGraph('#sentiment_div');

			$("#submit-status").html("<b>Status:</b> " + scriptName + " execution done");
			if (result.deployr.response.warnings) {
				console.log(result.deployr.response.warnings);
			}
		};
	};

	$('#analyze_btn').click(function() {
		$(".chart-container").show();
		$('html, body').animate({
			scrollTop: $('#analyze_btn').offset().top - 45
		}, 200);

		searchTerm = $('#search_input').val();
        Dajaxice.trends.add_user_keyword(function() {}, {"text":searchTerm});

		showLoading();
		var getStartDate = function(duration) {
			var d = new Date();
			d.setDate(d.getDate() - duration);
			return d.toISOString().slice(0, 10);
		};
		var getEndDate = function() {
			var d = new Date();
			return d.toISOString().slice(0, 10);
		};
		logIn('testuser', 'secret', function() {
			var generateGraphs = function() {
				return function(result) {
                $("#alert-container").empty();
				executeScript('OverallUsersScript',
				  [R.RDataFactory.createString('searchString', searchTerm),
							  R.RDataFactory.createString('startDate', getStartDate(7)),
							  R.RDataFactory.createString('endDate', getEndDate())
							  ], "res", $output_overall, overallComplete);
				executeScript('WordCloudUsersScript',
				  [R.RDataFactory.createString('searchString', searchTerm),
							  R.RDataFactory.createString('startDate', getStartDate(2)),
							  R.RDataFactory.createString('endDate', getEndDate())
							  ], "res", $output_wordcloud, wordcloudComplete);
				executeScript('IGraphUsersScript',
				  [R.RDataFactory.createString('searchString', searchTerm),
							  R.RDataFactory.createString('startDate', getStartDate(7)),
							  R.RDataFactory.createString('endDate', getEndDate())
							  ], "res", $output_graphing, igraphComplete);
				executeScript('SentimentUsersScript',
				  [R.RDataFactory.createString('searchString', searchTerm),
							  R.RDataFactory.createString('startDate', getStartDate(1)),
							  R.RDataFactory.createString('endDate', getEndDate())
							  ], "res", $output_sentiment, sentimentComplete);
				};
			};

			$("#alert-container").append('<div class="alert fade in alert-info"> <a data-dismiss="alert" href="#" class="close">×</a>Collecting data... Please be patient, if the search term is not cached, it could take up to 2 minutes.</div>');
			executeScript('InitializeFeed',
						  [R.RDataFactory.createString('searchString', searchTerm)], null, null, generateGraphs);
		});
	});

	window.project = null;
});
