$.wordcloud = {
	data:[],
	update_wordcloud: function() {
		var width = 450, height = 450;
		var fill = d3.scale.category20();
		d3.select("svg#wordcloud_graph").remove()
		var fontSize = d3.scale.log().range([6, 36]);
		d3.layout.cloud().timeInterval(10)
		.size([width, height])
		.words($.wordcloud.data)
		.rotate(function() { return ~~(Math.random() * 2) * 90; })
		//.rotate(function() { return ~~(Math.random() * 5) * 30 - 60; })
		.rotate(function() { return 0; })
		.font("Impact")
		.fontSize(function(d) { return fontSize(+d.size); })
		.on("end", draw)
		.start();

		function draw(words) {
			var vis = d3.select("#wordcloud_div").append("svg:svg").attr("id", function() { return "wordcloud_graph"; })
			.attr("class", function() { return "wordcloud"; })
			.append("g")
			.attr("transform", "translate(" + width/2 + "," + height/2 + ")");
			var text = vis.selectAll("text").data(words);

			text.transition().duration(1000)
			.attr("transform", function(d) {
				return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			}).style("font-size", function(d) {
				return d.size + "px";
			});

			text.enter().append("text")
			.style("font-size", function(d) { return d.size + "px"; })
			.style("font-family", "Impact")
			.style("fill", function(d, i) { return fill(i); })
			.attr("text-anchor", "middle")
			.transition().duration(1000)
			.attr("transform", function(d) {
				return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			})
			.text(function(d) { return d.text; });
		}
	}
}
