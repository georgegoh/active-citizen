from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
#from django.contrib.auth.decorators import login_required
from account.decorators import login_required

from django.views.generic import TemplateView
from views import HomeView, DashboardView, GraphsView

from django.contrib import admin
admin.autodiscover()

from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()

urlpatterns = patterns("",
    url(r"^$", HomeView.as_view(), name="home"),
    url(r"^privacy$", TemplateView.as_view(template_name="privacy.html"), name="privacy"),
    url(r"^terms$", TemplateView.as_view(template_name="terms.html"), name="terms"),
    url(r"^dashboard/", login_required(DashboardView.as_view()), name="dashboard"),
    url(r"^graphs/", login_required(GraphsView.as_view()), name="graphs"),
    url(r"^admin/", include(admin.site.urls)),

    url(r"^account/", include("account.urls")),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
