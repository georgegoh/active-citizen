#!/usr/bin/env python

from django.core.management.base import BaseCommand, CommandError
from twittertrends.models import Keyword

class Command(BaseCommand):
    args = '<source value>'
    help = 'Add a new keyword to the list'

    def handle(self, *args, **options):
        source = args[0]
        value = args[1]
        try:
            Keyword.objects.get(source=source, value=value)
        except Keyword.DoesNotExist:
            Keyword.objects.create(source=source, value=value)
      
