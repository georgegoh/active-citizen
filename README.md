Active Citizen
==============

Display current trending topics in Singapore taken from Google and Twitter. User can add custom keywords to generate word cloud graph, igraph and sentiment graph.


Features
========

* Google/Twitter trends retrieval
* Keyword-based charts/graphs:
    * Overall tweets graph
    * Word cloud
    * Basic sentiment analysis
    * Igraph


Usage
=====

* Installation
    Run 'pip install -r requirements.txt' to install the required packages. Package pinax-utils problably needs to be installed manually. It is recommended to run pip in a virtualenv.

* Initialization

    python manage.py syncdb

    python manage.py collectstatic


* Collecting data

    python manage.py addtrends google

    python manage.py addtrends twitter


	It is a good idea to run the above commands in cron jobs to update the database periodically.

* Deployment
    * Install Apache
    * Modify the sample Apache conf file config/active\_citizen.conf and put in httpd conf directory
	* Refer to script/install.sh for reference on how to set up
